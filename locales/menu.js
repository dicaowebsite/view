
module.exports = {
  menu:{
    0:{
      name:'home',
      url:'/'
    },
    1:{
      name:'product',
      url:'/'+this.$i18n.locale+'/product'
    },
    2:{
      name:'after_sales_service',
      url:'/'+this.$i18n.locale+'/services'
    },
    3:{
      name:'blog',
      url:'/'+this.$i18n.locale+'/blog'
    },
    4:{
      name:'contactus',
      url:'/'+this.$i18n.locale+'/contactus'
    },
  }
}
