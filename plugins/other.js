import Vue from 'vue'

Vue.prototype.$url = process.env.BASE_BACKEND+'api/';
Vue.prototype.$storage =process.env.BASE_BACKEND;
import vSelect from 'vue-select';

Vue.component('v-select', vSelect);
import 'vue-select/dist/vue-select.css';
